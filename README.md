# Sniffer for 802.15.4, 900 MHz

Uses the Atmel 212B tranceiver and works with the SAMD20 XPlained Pro dev kit.

Technically speaking this demo brings together the following pieces:

* LWMesh PHY layer, wrapped up in a library
* SAMD20 clock init, wrapped in a library
* DebOS bootloader and checkpoint functionality

The PHY library configures the tranceiver to be in "promiscious mode", which makes it give all received frames to the microcontroller.  The Lightweight Mesh PHY layer receives those frames and hands them to a single application function known as a "indication to the application" in 802.15.4 jargon.

That indication function (the code in this app) prepares a message containing the frame formatted to ASCII Hex bytes, and then calls the DebOS system checkpoint function, telling it to print out that message.

![shot.png](https://bitbucket.org/bootladder/DebOS_Sniffer/shot.png)

![snifferdemo.png](https://bitbucket.org/repo/LAERrz/images/2542367020-snifferdemo.png)

The screenshot shows the following:

* Application output consisting of 802.15.4 frames terminated by ====\n
* Tool used to halt the target, then it is shown that there is no output
* Tool used to load application on target
* Tool used to reset target
* Application output shown again
