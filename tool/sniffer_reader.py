import struct
from datetime import datetime

path_to_serial = '/dev/ttyUSB0'

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

def find_between_r( s, first, last ):
    try:
        start = s.rindex( first ) + len( first )
        end = s.rindex( last, start )
        return s[start:end]
    except ValueError:
        return ""

#TEST
#s = "1234====STRING====\n"
#print find_between( s, "====", "====" ) , "between"
#print find_between_r( s, "====", "====" ) , "between_r"


## Open the file with read only permit
f = open(path_to_serial)

while True:
	line = f.readline()
	csvtimestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
	print line
	payload = find_between( line, "====", "====" ) 
	print  payload
	channel = payload[0:2]
	rssi = payload[2:4]
	lqi = payload[4:6]
	realpayload = payload[6:]

	channel = struct.unpack('B', channel.decode('hex'))	
	rssi = struct.unpack('b', rssi.decode('hex'))	
	lqi = struct.unpack('B', lqi.decode('hex'))	
	print "channel: " , channel[0]
	print "rssi: " , rssi[0]
	print "lqi: " , lqi[0]
	print "realpayload : " , realpayload 

	csvline = "%s,%d,%d,%d,%s\n"%(csvtimestamp,channel[0],rssi[0],lqi[0],realpayload)
	print csvline
	outfile = open('data.csv','a')
	outfile.write(csvline)
	outfile.close()
f.close()
