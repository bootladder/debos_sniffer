import struct
from datetime import datetime
import sys

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

def find_between_r( s, first, last ):
    try:
        start = s.rindex( first ) + len( first )
        end = s.rindex( last, start )
        return s[start:end]
    except ValueError:
        return ""

#TEST
#s = "1234====STRING====\n"
#print find_between( s, "====", "====" ) , "between"
#print find_between_r( s, "====", "====" ) , "between_r"


## Open the file with read only permit
#f = open('/dev/ttyS4')
f = open(sys.argv[1])

while True:
	line = f.readline()
	csvtimestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
	print line
	payload = find_between( line, "====", "====" ) 
	print  payload
	channel = payload[0:2]
	rssi = payload[2:4]
	lqi = payload[4:6]
	realpayload = payload[6:]

	channel = struct.unpack('B', channel.decode('hex'))	
	rssi = struct.unpack('b', rssi.decode('hex'))	
	lqi = struct.unpack('B', lqi.decode('hex'))	
	print "channel: " , channel[0]
	print "rssi: " , rssi[0]
	print "lqi: " , lqi[0]
	print "realpayload : " , realpayload 
        payloadbytes = realpayload.decode('hex')
        print "MAC HEADER:"
        print "framecontrol: " , payloadbytes[0:2].encode('hex').upper()
        print "seqnum: " , payloadbytes[2:3].encode('hex').upper()
        print "PANID: " , payloadbytes[3:5].encode('hex').upper()
        print "DestAddr: " , payloadbytes[5:7].encode('hex').upper()
        print "SrcAddr: " , payloadbytes[7:9].encode('hex').upper()
        print "NWK HEADER:"
        print "framecontrol: " , payloadbytes[9:10].encode('hex').upper()
        print "seqnum: " , payloadbytes[10:11].encode('hex').upper()
        print "SrcAddr: " , payloadbytes[11:13].encode('hex').upper()
        print "DstAddr: " , payloadbytes[13:15].encode('hex').upper()
        print "SrcDestEPs: " , payloadbytes[15:16].encode('hex').upper()
        print "App Payload: " , payloadbytes[16:].encode('hex').upper()

	csvline = "%s,%d,%d,%d,%s\n"%(csvtimestamp,channel[0],rssi[0],lqi[0],realpayload)
	print csvline
	outfile = open('data.csv','a')
	outfile.write(csvline)
	outfile.close()
f.close()
