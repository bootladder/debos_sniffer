import struct
from datetime import datetime
import sys

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

def find_between_r( s, first, last ):
    try:
        start = s.rindex( first ) + len( first )
        end = s.rindex( last, start )
        return s[start:end]
    except ValueError:
        return ""


f = open(sys.argv[1])

while True:
	line = f.readline()
	csvtimestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
	print line
	payload = find_between( line, "====", "====" ) 
	channel = payload[0:2]
	rssi = payload[2:4]
	lqi = payload[4:6]
	realpayload = payload[6:]

	channel = struct.unpack('B', channel.decode('hex'))	
	rssi = struct.unpack('b', rssi.decode('hex'))	
	lqi = struct.unpack('B', lqi.decode('hex'))	
        print "RF LINK:"
	print "| chan | rssi | lqi | "
	print "| %02d   | %03d  | %03d | " % (channel[0], rssi[0], lqi[0])

        payloadbytes = realpayload.decode('hex')

        if payloadbytes[0] == '\x02':
            print 'PHY ACK\n'
            continue

        mac_framecontrol = payloadbytes[0:2].encode('hex').upper()
        mac_seqnum       = payloadbytes[2:3].encode('hex').upper()
        mac_PANID        = payloadbytes[3:5].encode('hex').upper()
        mac_DestAddr     = payloadbytes[5:7].encode('hex').upper()
        mac_SrcAddr      = payloadbytes[7:9].encode('hex').upper()

        nwk_framecontrol = payloadbytes[9:10].encode('hex').upper()
        nwk_seqnum       = payloadbytes[10:11].encode('hex').upper()
        nwk_SrcAddr      = payloadbytes[11:13].encode('hex').upper()
        nwk_DstAddr      = payloadbytes[13:15].encode('hex').upper()
        nwk_SrcDestEPs   = payloadbytes[15:16].encode('hex').upper()
        nwk_AppPayload  = payloadbytes[16:].encode('hex').upper()


        print "MAC HEADER:" , "                             ", "NWK HEADER:"
        print "| FC   | Seq | PAN | Dest | Src  |  " ,  \
                    "  | FC | Seq| Src  | Dst  | EPs "
        print ("| %s | %s | %s | %s | %s |   "  + \
                    "  | %s | %s | %s | %s | %s  | ")  \
            \
            % (mac_framecontrol, mac_seqnum, mac_PANID, mac_DestAddr, mac_SrcAddr,
             nwk_framecontrol, nwk_seqnum, nwk_SrcAddr, nwk_DstAddr,
                nwk_SrcDestEPs )

        print "APP PAYLOAD:"
        print "| " + nwk_AppPayload + " |"

        print 
        print 

f.close()
