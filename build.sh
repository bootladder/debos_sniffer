#!/bin/bash
mkdir -p build
echo building now!
if [ $1 = "-v" ]
then
	cd build; cmake .. && make VERBOSE=1
else
	cd build; cmake .. && make
fi

