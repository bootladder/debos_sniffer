CLEANUP = rm -f
MKDIR = mkdir -p

UNITY_ROOT=./link_to_unity_root
HOST_C_COMPILER=gcc
CROSS_C_COMPILER=arm-none-eabi-gcc
CROSS_AR=arm-none-eabi-ar
CROSS_SIZE=arm-none-eabi-size
OBJCOPY = arm-none-eabi-objcopy
SIZE = arm-none-eabi-size

CFLAGS = -std=c99
CFLAGS += -Wall -Wextra -Werror 
CFLAGS += -Wpointer-arith 
CFLAGS += -Wcast-align 
CFLAGS += -Wwrite-strings 
CFLAGS += -Wswitch-default 
CFLAGS += -Wunreachable-code
CFLAGS += -Winit-self 
CFLAGS += -Wmissing-field-initializers 
CFLAGS += -Wno-unknown-pragmas 
CFLAGS += -Wundef -Wold-style-definition -Wstrict-prototypes
CFLAGS += -Wmissing-prototypes -Wmissing-declarations
CFLAGS += -DUNITY_FIXTURES

CROSS_CFLAGS += -W -Wall --std=gnu99 -O0 -g
CROSS_CFLAGS += -fdata-sections -ffunction-sections
CROSS_CFLAGS += -funsigned-char -funsigned-bitfields
CROSS_CFLAGS += -mcpu=cortex-m0plus -mthumb

CROSS_LDFLAGS += -mcpu=cortex-m0plus -mthumb
CROSS_LDFLAGS += -Wl,--gc-sections
CROSS_LDFLAGS += -Wl,--script=./atsamd20j18.ld
CROSS_LDFLAGS += -Wl,-Map=output.map

######################################################

TARGET_CROSS_PRODUCTION = DeBos_Sniffer.elf
TARGET_CROSS_PRODUCTION_BIN = DeBos_Sniffer.bin

SRC_FILES_PRODUCTION=\
  app.c \
  halStartup.c \
  my_memcpy.c

SRC_FILES_PRODUCTION_MAIN=\

SRC_FILES_PRODUCTION_CROSSTARGET_ONLY=\

LIB_DIRS_ARGUMENT = -L../lib -L.
LIBS_ARGUMENT = -lSAMD20_Clock -lSAMD20_PHY_212B

INC_DIRS=-I. -I../lib -I../samd20_cmsis_headers -I../arm_cmsis_headers \
						 
all: production_cross clean

######################################################


######################################################
# CROSS COMPILES:  ONLY CREATE THE PRODUCTION EXECUTABLE

production_cross: $(SRC_FILES1)
	@echo Building production executable Cross compiled
	$(CROSS_C_COMPILER) $(CROSS_CFLAGS) $(INC_DIRS) $(SYMBOLS) \
			$(SRC_FILES_PRODUCTION) $(SRC_FILES_PRODUCTION_MAIN) \
				$(SRC_FILES_PRODUCTION_CROSSTARGET_ONLY) \
			$(CROSS_LDFLAGS) \
			$(LIB_DIRS_ARGUMENT) $(LIBS_ARGUMENT) \
				 -o $(TARGET_CROSS_PRODUCTION)

	@echo $(OBJCOPY) -O binary -R .eeprom $(TARGET_CROSS_PRODUCTION) $(TARGET_CROSS_BIN)
	@$(OBJCOPY) -O binary -R .eeprom $(TARGET_CROSS_PRODUCTION) $(TARGET_CROSS_PRODUCTION_BIN)
	@$(SIZE) $(TARGET_CROSS_PRODUCTION) 
	
######################################################
$(TARGET_CROSS_PRODUCTION_LIB): production_cross_objects
	@echo  -e '\n'$@'\n' Building production library for testing
	@echo $(CROSS_AR) rcs  $(TARGET_CROSS_PRODUCTION_LIB) *.o 
	@$(CROSS_AR) rcs  $(TARGET_CROSS_PRODUCTION_LIB) *.o 
######################################################
#create the object files for library
production_cross_objects: $(SRC_FILES1) 
	@echo -e '\n'$@'\n' Compiling tested production objects for TARGET 
	$(CROSS_C_COMPILER) $(CROSS_CFLAGS) $(INC_DIRS) $(SYMBOLS) \
			$(SRC_FILES_PRODUCTION)  \
				 -c

clean:
	$(CLEANUP) *.o



deploy:
	@sudo edbg -t atmel_cm0p -p -f $(TARGET_CROSS_PRODUCTION_BIN)
