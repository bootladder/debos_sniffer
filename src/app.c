#include "System.h"
#include "SAMD20_PHY_212B.h"
#include "SAMD20_Clock.h"
#include <stdint.h>
#include <stdbool.h>
#include "my_memcpy.h"
#include "halLed.h"
#include "halGpio.h"

#ifndef SNIFFER_CHANNEL
#define SNIFFER_CHANNEL 1
#else
#define XSTR(x) STR(x)
#define STR(x) #x
#pragma message "OVERRIDING SNIFFER CHANNEL FROM COMMANDLINE DEFINE"
#pragma message "CHANNEL IS " XSTR(SNIFFER_CHANNEL)
#endif

/*************************************************************** 
   Application Header and Checkpoint Pointer

   At the moment just a pointer located at 0x2000
   It points to the Application Function, and is called by the system
   Fwd Decl of app function necessary
*/

void app(void);

#define APP_HEADER_SECTION __attribute__ ((section(".app_header")))

APP_HEADER_SECTION void (*apppointer)(void) = &app;

void SystemCallTable_Init(void);

system_checkpoint_pointer_t *mycheckpointpointer;

SystemCallTable *systemCallTable;

/*************************************************************** 
   Application Function

   This function is called by the system while(1)
   This Hello World App only calls the checkpoint function
*/

static uint8_t buf[100]; //optional message buf for checkpoint
static int onetime;

uint8_t phybuf[120];
uint8_t phyptr;

uint8_t mychannel;

HAL_GPIO_PIN(DEBUG0, A, 10);

bool setChannelCalledFlag = false;


static void _1byte_to_2asciihexbytes(uint8_t onebyte, uint8_t onebyteformatted[2]);

static int setChannel(uint8_t sigNum, uint8_t *data);
static int setSecurityKey(uint8_t sigNum, uint8_t *data);

static void Init_HAL(void);

static void Register_SignalHandlers(void);

void app(void) {
    if (onetime != 0xdead) {
        onetime = 0xdead;

        Init_HAL();

        SystemCallTable_Init();
        Register_SignalHandlers();

        if (mycheckpointpointer) {
            //Flags: print address of checkpoint, print optional message, LED ON
            uint8_t flags = 0x2 | 0x10 | 0x8;
            my_memcpy(buf, "Sniffer Up: Channel X\n", sizeof("Sniffer Up: Channel X\n"));
            mychannel < 10 ? (buf[20] = '0' + mychannel) : (buf[20] = 'A');  //  A for 10
            checkpoint1:
            (mycheckpointpointer)((uint32_t) (uintptr_t) && checkpoint1, flags, buf);
        }
        return;
    }

    PHY_TaskHandler();

    if (setChannelCalledFlag) {
        setChannelCalledFlag = false;
        //Call the checkpoint function
        uint8_t flags = 0x2 | 0x10 | 0x8;

        uint8_t msg[100] = "Set Channel:   ";
        msg[13] = '0' + mychannel;

        if (mycheckpointpointer)
            checkpoint3:
            (mycheckpointpointer)((uint32_t) (uintptr_t) && checkpoint3, flags, (uint8_t *) msg);

    }
}

static void Register_SignalHandlers(void){
    SignalHandler signalHandlerSetChannel;
    signalHandlerSetChannel.signalHandlerFunc = setChannel;
    signalHandlerSetChannel.name = "Set Channel Number";
    signalHandlerSetChannel.description = "ASCII Integer Argument.  A response will be sent as a checkpoint";

    systemCallTable->System_SetSignalHandler_Pointer(1, signalHandlerSetChannel);

    SignalHandler signalHandlerSetSecurityKey;
    signalHandlerSetChannel.signalHandlerFunc = setSecurityKey;
    signalHandlerSetChannel.name = "Set Security Key";
    signalHandlerSetChannel.description = "ASCII Integer Argument.  A response will be sent as a checkpoint";

    systemCallTable->System_SetSignalHandler_Pointer(2, signalHandlerSetSecurityKey );
}

static void Init_HAL(void) {
    Clock_Init();
    halPhyInit();
    PHY_Init();

    mychannel = SNIFFER_CHANNEL;
    PHY_SetShortAddr(0);
    PHY_SetPanId(0x4567);
    PHY_SetChannel(mychannel);
    PHY_SetBand(0);
    PHY_SetModulation(0x24);
    PHY_SetRxState(true);


    HAL_LedInit();
    HAL_LedOn(0);

    HAL_GPIO_DEBUG0_out();
    HAL_GPIO_DEBUG0_set();
}
/***************************************************************/


//Required to be here by the stack.  Unused
void PHY_DataConf(uint8_t status) {
    (void) status;
}

static void _1byte_to_2asciihexbytes(uint8_t onebyte, uint8_t onebyteformatted[2]) {
    onebyteformatted[0] = onebyte >> 4;
    if (onebyteformatted[0] < 10)
        onebyteformatted[0] = '0' + onebyteformatted[0];
    else
        onebyteformatted[0] = 'A' + (onebyteformatted[0] - 10);

    onebyteformatted[1] = onebyte & 0x0F;
    if (onebyteformatted[1] < 10)
        onebyteformatted[1] = '0' + onebyteformatted[1];
    else
        onebyteformatted[1] = 'A' + (onebyteformatted[1] - 10);
}


void PHY_DataInd(PHY_DataInd_t *ind) {
    HAL_GPIO_DEBUG0_toggle();


    phyptr = 0;
    uint8_t twoasciibytes[2];

    my_memcpy(&(phybuf[phyptr]), "====", 4);
    phyptr += 4;

    _1byte_to_2asciihexbytes(mychannel, twoasciibytes);
    phybuf[phyptr++] = twoasciibytes[0];
    phybuf[phyptr++] = twoasciibytes[1];
    _1byte_to_2asciihexbytes(ind->rssi, twoasciibytes);
    phybuf[phyptr++] = twoasciibytes[0];
    phybuf[phyptr++] = twoasciibytes[1];
    _1byte_to_2asciihexbytes(ind->lqi, twoasciibytes);
    phybuf[phyptr++] = twoasciibytes[0];
    phybuf[phyptr++] = twoasciibytes[1];

    for (uint8_t i = 0; ((i < ind->size) && (i < 40)); i++) {
        _1byte_to_2asciihexbytes(ind->data[i], twoasciibytes);

        phybuf[phyptr++] = twoasciibytes[0];
        phybuf[phyptr++] = twoasciibytes[1];
    }
    my_memcpy(&(phybuf[phyptr]), "====\n\0", 6);

    //print address of checkpoint, print optional message, LED ON
    uint8_t flags = 0x2 | 0x10 | 0x8;

    //Call the checkpoint function
    if (mycheckpointpointer)
        checkpoint2:
        (mycheckpointpointer)((uint32_t) (uintptr_t) && checkpoint2, flags, phybuf);

    ind->data = 0;
}

/* Incoming data is in ASCII
 */
static int setChannel(uint8_t sigNum, uint8_t *data) {
    (void) sigNum;
    mychannel = (*data) - '0';
    PHY_SetChannel(mychannel);

    setChannelCalledFlag = true;
    return 0;
}

static int setSecurityKey(uint8_t sigNum, uint8_t *data) {
    (void) sigNum; (void) data;
    PHY_EncryptReq((uint8_t *) 0, (uint8_t *) 0 );

    setChannelCalledFlag = true;
    return 0;
}



/***************************************************************/
static void System_Checkpoint_Init(void) {
    mycheckpointpointer = (systemCallTable->System_Checkpoint_Pointer);
}

void SystemCallTable_Init(void) {
    //first dreference the address of checkpoint pointer
    uint32_t *addrptr = (uint32_t *) 0x1FF0;

    systemCallTable =
            (SystemCallTable *) addrptr;

    System_Checkpoint_Init();
}
/***************************************************************/
// main() : For the bare metal version, this is the actual while(1)
//
// For bootloaded version this is a dummy main.  
//		Use it to get the app to compile and not optimize stuff
//
int main(void) {
    //This line is very important, it forces apppointer to exist in the binary.
    if (0 == apppointer)
        return 1;

    while (1)
        app();
}
